NS Admin Tools
================================

This repository contains tools (currently just one) for NS Admin usage. So far, I have supplied NS Admins with code to find nations that have problems with them.

Zero of Zero
-------------------------

I previously gave admins a shell script to find *0 of 0* nations. It has not been added to this repository (yet).

WA Members API errors
-------------------------

One can run wa\_members\_validator.py to produce a bbcode formatted listing of nations for which the last major update listed the correct WA Status but the WA Members list (accessible through the API) is wrong, including a list of nations listed erroneously (with non-existent nations specially marked) with links to where they are in the browsable WA Members list and a list of nations erroneously *not* listed.
